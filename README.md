# Projet Cassé
Ce projet contient un petit projet react un peu pété. Pour voir les choses à faire pour le rendre mieux, aller voir la liste des issues !

## Comment lancer le projet
* `npm i` pour installer les dépendances
* `npm start` pour lancer le projet sur localhost:3000 et un fake backend sur localhost:4000

## Comment Participer à l'améliration du projet
* Faire un fork de ce projet
* Sur son fork, créer une branche dédiée à la feature choisie
* Une fois qu'on a terminé de coder sa feature, on fait push sur son fork puis sur gitlab on va faire une nouvelle Merge Request depuis sa branche feature de son fork vers la branche develop du projet original